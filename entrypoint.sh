#!/bin/sh

set -e

envsubst < /etc/eginx/default.conf.tpl > /etc/nginx/conf.d/default.conf
nginx -g 'daemon off;'